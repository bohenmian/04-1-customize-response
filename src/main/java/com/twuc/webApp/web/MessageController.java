package com.twuc.webApp.web;

import com.twuc.webApp.model.Message;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class MessageController {

    @GetMapping("/no-return-value")
    public void noReturnValue() {

    }

    @GetMapping("/no-return-value-with-annotation")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void noReturnValueWithAnnotation() {

    }

    @GetMapping("/messages/{message}")
    public String getMessage(@PathVariable("message") String message) {
        return message;
    }

    @GetMapping("/message-objects/{message}")
    public Message getMessageObject(@PathVariable String message) {
        return new Message(message);
    }

    @GetMapping("/message-objects-with-annotation/{message}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public Message getMessageWithAnnotation(@PathVariable String message) {
        return new Message(message);
    }

    @GetMapping("/message-entities/{message}")
    public ResponseEntity<Message> messageResponseEntity(@PathVariable String message) {
        return ResponseEntity.status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .header("X-Auth", "me")
                .body(new Message(message));
    }

}
