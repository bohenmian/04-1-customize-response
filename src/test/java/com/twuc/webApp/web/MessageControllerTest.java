package com.twuc.webApp.web;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.twuc.webApp.model.Message;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
public class MessageControllerTest {

    @Autowired
    private MockMvc mockMvc;
    private ObjectMapper objectMapper;

    @BeforeEach
    void setUp() {
        objectMapper = new ObjectMapper();
    }

    @Test
    void should_return_the_default_code() throws Exception {
        mockMvc.perform(get("/api/no-return-value"))
                .andExpect(status().isOk());
    }

    @Test
    void should_return_the_status_code_is_204() throws Exception {
        mockMvc.perform(get("/api/no-return-value-with-annotation"))
                .andExpect(status().isNoContent());
    }

    @Test
    void should_return_the_message_and_code() throws Exception {
        mockMvc.perform(get("/api/messages/hello"))
                .andExpect(status().isOk())
                .andExpect(content().contentType("text/plain;charset=UTF-8"));
    }

    @Test
    void should_return_the_object() throws Exception {
        mockMvc.perform(get("/api/message-objects/message"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.value").value("message"));
    }

    @Test
    void should_return_the_status_code_and_response() throws Exception {
        mockMvc.perform(get("/api/message-objects-with-annotation/message"))
                .andExpect(status().isAccepted())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.value").value("message"));
    }

    @Test
    void should_return_response_entity_contain_header_and_code() throws Exception {
        mockMvc.perform(get("/api/message-entities/message"))
                .andExpect(status().isOk())
                .andExpect(header().string("X-Auth", "me"))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath(".value").value("message"));
    }
}
